---

# define variables for this current iteration (just for convenience, since extracting
# values from a list of dicts like this is rather verbose, as you can see)
- name: Define variables for {{ nci.name }}
  ansible.builtin.set_fact:
    this_nci_conf_dbname: "{{ nci.config | selectattr('name', 'match', 'dbname') | map(attribute='value') | first }}"
    this_nci_conf_dbtype: "{{ nci.config | selectattr('name', 'match', 'dbtype') | map(attribute='value') | first }}"
    this_nci_conf_dbhost: "{{ nci.config | selectattr('name', 'match', 'dbhost') | map(attribute='value') | first }}"
    this_nci_conf_dbport: "{{ nci.config | selectattr('name', 'match', 'dbport') | map(attribute='value') | first }}"
    this_nci_conf_dbuser: "{{ nci.config | selectattr('name', 'match', 'dbuser') | map(attribute='value') | first }}"
    this_nci_conf_dbpass: "{{ nci.config | selectattr('name', 'match', 'dbpassword') | map(attribute='value') | first }}"
    this_nci_conf_utf8mb4: "{{ nci.config | selectattr('name', 'match', 'mysql.utf8mb4') | map(attribute='value') | first }}"
    this_nci_conf_datadir: "{{ nci.config | selectattr('name', 'match', 'datadirectory') | map(attribute='value') | first }}"
    this_nci_conf_cachedir: "{{ nci.config | selectattr('name', 'match', 'cache_path') | map(attribute='value') | first }}"

# I have decided to use the presence of config/config.php
# as a canary for an already working Nextcloud installation
- name: "Check if instance {{ nci.name }} is already installed"
  ansible.builtin.stat:
    path: "{{ nci.www.dir }}/config/config.php"
  register: nextcloud_installed

- name: This is a fresh install
  ansible.builtin.set_fact:
    nextcloud_fresh_install: "{{ not (nextcloud_installed.stat.exists | bool) }}"

# download and unpack archive if this is a fresh install
# but skip this task if backup has been requested
- name: "Download and unpack Nextcloud v{{ nci.version }}"
  ansible.builtin.include_tasks: loop/install.yml
  when:
    - nextcloud_fresh_install | bool
    - not (nextcloud_backup | bool)

# this task is skipped if backup has been requested, no need to pollute
# the backup if these dirs did not already exist
- name: Create Nextcloud's cache and data directories
  ansible.builtin.file:
    path: "{{ item.path }}"
    state: directory
    owner: "{{ nci.www.user }}"
    group: "{{ nci.www.user }}"
    mode: "{{ item.mode }}"
  loop:
    - { path: "{{ this_nci_conf_cachedir }}", mode: '0755' }
    - { path: "{{ this_nci_conf_datadir }}",  mode: '0770' }
  when: not (nextcloud_backup | bool)

# database is only created if not already existing
- name: Create Nextcloud's SQL database and SQL user
  ansible.builtin.include_tasks: loop/database.yml

# this task is only run if "nextcloud_configure == TRUE" or if we think this
# is a fresh install
- name: Initialise Nextcloud's database and config.php
  ansible.builtin.include_tasks: loop/config.yml
  tags: nextcloud-config
  when: >
    (nextcloud_configure | bool) or
    (nextcloud_fresh_install | bool)

- name: Set ownership and permissions on directories and files
  ansible.builtin.include_tasks: loop/perms.yml

# this task is skipped if backup has been requested
- name: Setup Apache virtualhost
  ansible.builtin.include_tasks: loop/vhost.yml
  when: not (nextcloud_backup | bool)

# this task is skipped if backup has been requested
- name: Setup systemd jobs
  ansible.builtin.include_tasks: loop/systemd.yml
  when: not (nextcloud_backup | bool)


# Backup tasks are only executed if "nextcloud_backup == TRUE" or "nextcloud_upgrade == TRUE"
# This is my way of forcing you to make a backup before any upgrades
- name: Backup
  when: (nextcloud_backup | bool) or (nextcloud_upgrade | bool)
  block:

    - name: Check that the backup target directory is reachable
      ansible.builtin.stat:
        path: "{{ nextcloud_backup_dir }}"
      register: nc_backup_dir

    # display informative message if the target dir is NOT reachable
    # to explain why backup was skipped
    - ansible.builtin.debug:
        msg: "Backup target was not reachable on the target. SKIPPING all backup tasks."
      when: not (nc_backup_dir.stat.exists | bool)

    - name: "Backup instance {{ nci.name }}"
      ansible.builtin.include_tasks: loop/backup.yml
      when:
        - nc_backup_dir.stat.exists | bool
        # if nextcloud_backup=True and "nextcloud_backup_instance: []" we backup *all* instances
        - nextcloud_backup_instance is search(nci.name) or (nextcloud_backup_instance | length == 0)
  # END OF BLOCK


- name: Upgrade {{ nci.name }}
  ansible.builtin.include_tasks: loop/upgrade.yml
  when: nextcloud_upgrade | bool
