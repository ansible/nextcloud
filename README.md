# Nextcloud

This Ansible role was tailored to run multiple Nextcloud instances on a single host.
You can of course also use it to run just a single Nextcloud instance.

The host running the Nextcloud instance(s) is assumed to run behind another
host handling reverse proxy TLS termination (I included an example vhost
configuration in `files/nextcloud.conf.j2`).

This role downloads and configures a fresh Nextcloud installation from scratch (completely
bypassing the web-gui configuration of database settings etc.) if it detects no
existing Nextcloud installation or if `--extra-vars "{nextcloud_configure: true}"`.

This role can make backups of the Nextcloud `www-root` directory, the Nextcloud
SQL database, and the Nextcloud data directory.
Backup is not performed unless you set `--extra-vars "{nextcloud_backup: true }"`.

In addition, if you set `nextcloud_backup_key` this role encrypts the data backup
using your GPG key (the key is assumed to reside on the Ansible controller).

This role can perform Nextcloud upgrades (you have to set `--extra-vars "{nextcloud_upgrade: true}"`).
Note that this executes the backup tasks *before* running the upgrade ones.


## Implicit dependencies

Assuming you run Nextcloud and all its dependencies on the same machine,
to run this role you will also need to have configured the following services
(I use the hyperlinked Ansible roles to achieve that, also see example playbook below).

+ [MariaDB Ansible role](https://codeberg.org/ansible/mariadb)
+ [PHP Ansible role](https://codeberg.org/ansible/php)
+ [PHP-versions Ansible role](https://codeberg.org/ansible/php-versions)
+ [Apache server Ansible role](https://codeberg.org/ansible/apache)
+ [Redis Ansible role](https://codeberg.org/ansible/redis)


## Example playbook

```
- name: Nextcloud instances
  hosts: all

  tasks:
    - { import_role: { name: mariadb },      become: true, tags: mariadb }
    - { import_role: { name: php-versions }, become: true, tags: php }
    - { import_role: { name: php },          become: true, tags: php }
    - { import_role: { name: apache },       become: true, tags: apache }
    - { import_role: { name: redis },        become: true, tags: redis }
    - { import_role: { name: nextcloud },    become: true, tags: nextcloud }
```


## Variables

For this role to be useful in your environment you need to specify one or more
Nextcloud instances and their desired configuration. For example, I manage
three instances on the same host, and have constructed the variables the following way.

All instances have the SQL database server in common, so in host/group vars:
```
nextcloud_database:
  socket: "/var/run/mysqld/mysqld.sock"
  host: "localhost"
  port: 3306
  type: "mysql"
  user:
    name: nextcloud
    pass: "{{ lookup('community.general.passwordstore', 'nextcloud/mariadb/user') }}"
  root:
    name: root
    pass: "{{ lookup('community.general.passwordstore', 'nextcloud/mariadb/root') }}"
```

For each instance I define a list item like this:
```
nextcloud_instance_NN:
  - name: nextcloud.example.se
    version: 29.0.7
    superuser:
      name: admin
      pass: "{{ lookup('community.general.passwordstore', 'nextcloud/example.se/users/admin') }}"
      email: "webmaster@example.se"
    www:
      user: www-data
      dir: "/var/www/html/nextcloud.example.se"
    config:
      - { name: 'redis host',        value: '/var/run/redis/redis-server.sock' }
      - { name: 'redis port',        value: "0", type: "integer" }
      - { name: "dbname",            value: "nextcloud_example" }
      - { name: "dbtype",            value: "{{ nextcloud_database.type }}" }
      - { name: "dbhost",            value: "{{ nextcloud_database.host }}" }
      - { name: "dbport",            value: "{{ nextcloud_database.port }}", type: "integer" }
      - { name: "dbuser",            value: "{{ nextcloud_database.user.name }}" }
      - { name: "dbpassword",        value: "{{ nextcloud_database.user.pass }}" }
      - { name: 'mysql.utf8mb4',     value: 'true', type: "boolean" }
      - { name: "datadirectory",     value: "/var/www/html/nextcloud.example.se/data" }
      - { name: "cache_path",        value: "/var/cache/nextcloud.example.se" }
      - { name: 'memcache.local',    value: '\\OC\\Memcache\\Redis' }
      - { name: 'memcache.locking',  value: '\\OC\\Memcache\\Redis' }
      - { name: "overwrite.cli.url", value: "https://nextcloud.example.se" }
      - { name: "overwritehost",     value: "nextcloud.example.se" }
      - { name: "overwriteprotocol", value: "https" }
    config_trusted_proxies:
      - "127.0.0.1"
      - "::1"
    config_trusted_domains:
      - "localhost"
      - "nextcloud.example.se"
    systemd:
      - name: Nextcloud cronjob timer
        service:
          dest: nextcloud.example.se-cron.service
          template: cron.service.j2
        timer:
          dest: nextcloud.example.se-cron.timer
          template: cron.timer.j2
      - name: Nextcloud preview generator
        service:
          dest: nextcloud.example.se-preview.service
          template: preview.service.j2
        timer:
          dest: nextcloud.example.se-preview.timer
          template: preview.timer.j2
```

Finally I create the list `nextcloud_instance` that this role expects:
```
nextcloud_instance: "{{ nextcloud_instance_01 + nextcloud_instance_02 + ... + nextcloud_instance_NN }}"
```



## Links and notes

+ https://github.com/nextcloud/documentation/blob/master/admin_manual/configuration_server/occ_command.rst
+ https://docs.nextcloud.com/server/stable/admin_manual/configuration_server/occ_command.html
+ https://www.digitalocean.com/community/tutorials/how-to-install-and-configure-nextcloud-on-ubuntu-20-04
+ https://open-netlab.de/nextcloud-v21-02-occ-commands


### Ansible roles

+ https://github.com/nextcloud/ansible-collection-nextcloud-admin
+ https://codeberg.org/mdik/ansible-nextcloud
+ https://git.coop/webarch/nextcloud
