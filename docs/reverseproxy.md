# Reverse proxy with TLS termination

This role assumes it is running behind a reverse proxy handling TLS termination.
The configuration of the reverse proxy is outside the scope of this role,
but for reference, below is the vhost I use on Apache 2.4.29 on Ubuntu Bionic.

For more web-server specific settings, my [`apache` Ansible role](https://codeberg.org/ansible/apache)
might be useful. As with all things web server, make sure you understand the security
implications of what you are doing.


## Example

Here's an example virtualhost configuration on the proxy (as Jinja template file):
```
<VirtualHost *:80>
   ServerAdmin {{ vhost_email }}
   ServerName {{ nextcloud_host }}
	
   Redirect permanent "/" "https://{{ nextcloud_host }}/"

   ErrorLog ${APACHE_LOG_DIR}/{{ nextcloud_host }}_error.log
   CustomLog ${APACHE_LOG_DIR}/{{ nextcloud_host }}_access.log combined
</VirtualHost>

<VirtualHost *:443>
   ServerAdmin {{ vhost_email }}
   ServerName {{ nextcloud_host  }}

   ProxyPreserveHost On
   ProxyRequests Off
   ProxyPass / http://{{ vhost_ip }}:{{ vhost_port }}/
   ProxyPassReverse / http://{{ vhost_ip }}:{{ vhost_port }}/

   RewriteEngine On
   RewriteRule ^/\.well-known/carddav https://%{SERVER_NAME}/remote.php/dav/ [R=301,L]
   RewriteRule ^/\.well-known/caldav https://%{SERVER_NAME}/remote.php/dav/ [R=301,L]

   SSLEngine on
   SSLCertificateKeyFile   /etc/letsencrypt/live/{{ nextcloud_host }}/privkey.pem
   SSLCertificateFile      /etc/letsencrypt/live/{{ nextcloud_host }}/cert.pem
   SSLCertificateChainFile /etc/letsencrypt/live/{{ nextcloud_host }}/chain.pem

   ErrorLog ${APACHE_LOG_DIR}/{{ nextcloud_host }}_error.log
   CustomLog ${APACHE_LOG_DIR}/{{ nextcloud_host }}_access.log combined
</VirtualHost>
```

Example Ansible tasks to run the above Jinja template:
```
- name: "Configure Nextcloud Apache vhost on the reverse proxy"
  block:

    - name: Set facts for Nextcloud instances
      set_fact:
        vhost_ip: 10.252.116.7 # LXD container
        vhost_port: 80
        vhost_email: "{{ nextcloud_email }}"
    
    - name: "Check for existing Certbot certificate"
      ansible.builtin.stat:
        path: "/etc/letsencrypt/live/{{ nextcloud_host }}/cert.pem"
      register: vhost_cert

    - name: "Fetch Certbot certificate for {{ nextcloud_host }}"
      ansible.builtin.command: >
        certbot certonly --noninteractive --agree-tos
        --preferred-challenges=http --authenticator standalone
        --pre-hook "systemctl stop apache2.service"
        --post-hook "systemctl start apache2.service"
        --email {{ vhost_email }}
        -d {{ nextcloud_host }}
      when: not (vhost_cert.stat.exists | bool)

    - name: "Configure virtualhost for {{ nextcloud_host }}"
      ansible.builtin.template:
        src: nextcloud.conf.j2
        dest: "/etc/apache2/sites-available/{{ nextcloud_host }}.conf"
        owner: root
        group: root
        mode: u=rw,go=r
      notify: reload apache
    
    - name: "Check whether the Nextcloud site is already enabled"
      ansible.builtin.stat:
        path: "/etc/apache2/sites-enabled/{{ nextcloud_host }}.conf"
      register: vhost_enabled
    
    - name: "Enable site (a2ensite) {{ nextcloud_host }}.conf"
      ansible.builtin.command: "a2ensite {{ nextcloud_host }}.conf"
      when: not (vhost_enabled.stat.exists | bool)
```



## Refs

+ https://docs.nextcloud.com/server/20/admin_manual/configuration_server/reverse_proxy_configuration.html
